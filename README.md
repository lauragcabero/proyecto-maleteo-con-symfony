Este proyecto ha sido clonado del repositorio de UpgradeHub PHP 'symfony-skeleton' y partiendo de ahí, he ido
creando el proyecto hasta el punto 3 incluído.

1. He creado una landing page mediante plantillas twig partiendo de una maquetación en HTML y CSS tradicional.
Para usar las plantillas twig con diferentes rutas he creado rutas absolutas para css e imágenes y ya no fallan al cambiar de ruta en la url.
La plantilla base contiene lo que comparten todas que es el header y el footer.

Una vez linkeado todo creé un controlador llamado DemoController.php donde mediante una ruta llamada /maleteo, tal como pedía el ejercicio, el método showLandingPage se encarga de pasarle con el método render a la plantilla indicada (home) el contenido de la landing page estática y mostrarla en el navegador.

2. El segundo ejercicio pedía que mediante el formulario de 3 input's se fuesen introduciendo las solicitudes de demo a una bbdd, y para ello he utilizado Doctrine de manera automática.

He ido creando con los comandos las Entity's Demo y Reviews (que la veremos en el punto 3) y después con migration, migrations:migrate conectando, preparando y subiendo a la bbdd que venía dada en el docker-compose.yml 'demo'. (Y sí, he llamado la Entity igual que la bbdd...errores de principiantes).

En este caso, modificando el DemoController.php y la @Route a /maleteo/submit, que antes solo renderizaba la web, usando EntityManagerInterface para interactuar con la bbdd y Request para 'recoger' los datos del formulario, estos datos los seteamos en la entidad Demo, instanciada ya también, y se los pasamos al persist para que los prepare para irse a la bbdd mediante el flush después.

Finalizamos este método render también para que se renderice la plantilla twig 'form-response' devolviendo el mensaje que contiene + el nombre del usuario que se lo pasamos en el valor del array y así compruebo que los datos se recibían bien al darle a submit, ya que la ruta declarada en el 'action' del 'form' la he modificado en este caso a maleteo/submit.

Después se hace la consulta a la bbdd demo y comprobamos que los datos se van subiendo correctamente tras cada submit.

3. Y finalmente en el ejercicio 3 se pedía que se dinamizase la parte de los comentarios para que en vez de aparecer estáticos con html.twig, salieran de una bbdd y los 3 últimos siempre apareciesen en el landing page.

Bien, pues para ello creé otra Entity llamada Reviews con los 4 campos que se pedían también. Y aquí igual, una vez creada la Entity: migration, migrations:migrate y bbdd lista para insertarle valores (que en mi caso ha sido a mano).

Aprovechando el controller LandingPage, y sobre la ruta principal del home /maleteo, el método showLandingPage ahora también se va a encargar de obtener todas las reviews de la bbdd mediante findAll con los métodos getDoctrine y getRepository para conectar con esta.

Pero como se pedía que fueran los 3 últimos, por ejemplo, le he aplicado el método de php 'array-slice' y es lo que le paso siempre al render, para que siempre muestre en la landing page las  3 últimas reviews que hayan en la bbdd en un array, como siempre junto al nombre de la correspondiente plantilla twig que renderiza esto.

También he creado una plantilla twig que solo se encarga de pasarle a su plantilla padre (que es home) mediante el método twig 'include' + lo que quieras incluir de la plantilla hija, que en este caso ha sido la parte de los comentarios solamente, que es dinámica y tiene un foreach que va pasando la información a los campos y los va rellenando con un condicional al principio que se encarga de mostrarlo solo si hay opiniones, claro.
